<?php

namespace StuffAndyMakes\ConsoleHelpers;

/*
 *  Console - Simple console output helper class
 */
class Console {

	/*
	 *  Use this for terminating with \n like this...
	 *    $con->print("blah blah blah" . $con::NL);
	 */
	public const NL = "\n";
	public const ESC = "\e[";

	private $controlCodes = [
		'cursorHome' => 'H',
		'cursorUp' => 'A',
		'cursorDown' => 'B',
		'cursorRight' => 'C',
		'cursorLeft' => 'D',
		'eraseToEndOfLine' => 'K',
		'eraseToStartOfLine' => '1K',
		'eraseLine' => '2K',
		'eraseToBottomOfScreen' => 'J',
		'eraseToTopOfScreen' => '1J',
		'eraseScreen' => '2J'
	];

	function __construct(bool $clearScreen = false) {
		if ($clearScreen) {
			$this->print(Console::ESC . $this->controlCodes['eraseScreen']);
			$this->print(Console::ESC . $this->controlCodes['cursorHome']);
		}
	}
	
	public function moveCursor($row = 0, $col = 0) {
		if ($row == 0 && $col == 0) {
			$this->control('cursorHome');
		} else {
			$this->print(Console::ESC . $row . ';' . $col . 'H');
		}
	}

	public function cursorUp($count = 1) {
		$this->print(Console::ESC . $count . $this->controlCodes['cursorUp']);
	}

	public function cursorDown($count = 1) {
		$this->print(Console::ESC . $count . $this->controlCodes['cursorDown']);
	}

	/*
	 *  Print a message WITHOUT a newline at the end to STDOUT
	 */
	public function print( $message = "" ) {
		fwrite( STDOUT, $message );
	}

	/*
	 *  Print a message with a newline at the end to STDOUT
	 */
	public function println( $message = "" ) {
		fwrite( STDOUT, $message . "\n" );
	}

	/*
	 *  Print an error message WITHOUT a newline at the end to STDERR
	 */
	public function error( $message = "" ) {
		fwrite( STDERR, $message );
	}

	/*
	 *  Print an error message with a newline at the end to STDERR
	 */
	public function errorln( $message = "" ) {
		fwrite( STDERR, $message . "\n" );
	}

	/*
	 * Convenience method to output a bright green "OK!" + a message
	 */
	public function OK( $message = "" ) {
		$this->println( CS::BoldGreen("OK!") . " " . $message );
	}

	/*
	 * Convenience method to output a bright green "Yes!" + a message
	 */
	public function YES( $message = "" ) {
		$this->println( CS::BoldGreen("Yes!") . " " . $message );
	}

	/*
	 * Convenience method to output a bright red "No!" + a message
	 */
	public function NO( $message = "" ) {
		$this->println( CS::BoldRed("No!") . " " . $message );
	}

	/*
	 * Convenience method to output a bright yellow "FYI:" + a message
	 */
	public function FYI( $message = "" ) {
		$this->println( CS::BoldYellow("FYI:") . " " . $message );
	}

	/*
	 * Convenience method to output a bright red "Error!" + a message
	 */
	public function ERR( $message = "" ) {
		$this->errorln( CS::BoldRed("Error!") . " " . $message );
	}

	/*
	 * Get a keypress in the terminal. Doesn't print out the character.
	 */
	public function getChar() {
		// Save existing tty configuration
		$term = `stty -g`;
		// this will make the system immediately give us a keypress
		system("stty -icanon -echo");
		$c = fread(STDIN, 1);
		// Reset the tty back to the original configuration
		system("stty '" . $term . "'");
		return $c;
	}

	public function getPassword() {
		// Save existing tty configuration
		$term = `stty -g`;
		// this will make the system immediately give us a keypress
		system("stty icanon -echo");
		$password = '';
		do {
			$c = fread(STDIN, 1);
			$password .= $c;
		} while ($c != "\n");
		// Reset the tty back to the original configuration
		system("stty '" . $term . "'");
		fwrite(STDOUT, "\n");
		return trim($password);
	}

	public function yesOrNo($message = 'Are you sure?') {
		$this->print($message . ' (y/N) ');
		do {
			$c = strtolower($this->getChar());
			if ($c == "\n") {
				$c = "n";
			}
		} while (strpos('yn', $c) === FALSE);
		$this->println($c == 'y' ? CS::BoldGreen('Yes') : CS::BoldRed('No'));
		return ($c == 'y');
	}
}
