<?php

// setup for autoload of classes the PSR-4 way
require "vendor/autoload.php";

use StuffAndyMakes\ConsoleHelpers\Console;
use StuffAndyMakes\ConsoleHelpers\CS;
  
/*
 *  Simple examples for using the Console() class with the CS (Color String) class
 */
$con = new Console(true);

$con->println('Print a line with a ' . CS::Purple('newline (\n)') . ' at the end.');

$con->print('This line does not have a newline, ');
$con->println('but this one does!');

$con->errorln('This will output to STDERR with a newline.');
$con->error('This goes to STDERR without a newline.');
$con->error("\n"); // terminates it with a newline

$con->print('Another way to add a newline.' . $con::NL);

$con->println();

$con->OK('This is an OK message!');
$con->ERR('This is an ERR message. :(');
$con->YES('This is an YES message!');
$con->NO('This is an NO message!');
$con->FYI('This is an FYI message.');

$con->print('Type a character: ');
$ch = $con->getChar();
$con->println();
$con->FYI('You typed ' . CS::BoldWhite($ch) . '.');

$con->print('Please type a secret: ');
$pw = $con->getPassword();
$con->FYI('You typed ' . CS::BoldYellow($pw));

if ($con->yesOrNo('Are you sure?')) {
    $con->OK('You are sure!');
} else {
    $con->ERR('You are NOT sure.');
}

// $con->println('This line will be replaced...');
// $con->getChar();
// $con->cursorUp();
// $con->

$con->println();

/*
 *  Just a simple test to show the color output of most of the combos
 */
$sample = 'ABCDEFG12345';

$con->print( CS::UnderlinedWhite('Dim         ') . ' ' );
$con->print( CS::UnderlinedWhite('Normal      ') . ' ' );
$con->print( CS::UnderlinedWhite('Bold        ') . ' ' );
$con->print( CS::UnderlinedWhite('Underline   ') . ' ' );
$con->println( CS::UnderlinedWhite('Blinking    ') );

$con->print( CS::DimBlack($sample) . ' ' );
$con->print( CS::Black($sample) . ' ' );
$con->print( CS::BoldBlack($sample) . ' ' );
$con->print( CS::UnderlinedBlack($sample) . ' ' );
$con->println( CS::BlinkingBlack($sample) );

$con->print( CS::DimRed($sample) . ' ' );
$con->print( CS::Red($sample) . ' ' );
$con->print( CS::BoldRed($sample) . ' ' );
$con->print( CS::UnderlinedRed($sample) . ' ' );
$con->println( CS::BlinkingRed($sample) );

$con->print( CS::DimGreen($sample) . ' ' );
$con->print( CS::Green($sample) . ' ' );
$con->print( CS::BoldGreen($sample) . ' ' );
$con->print( CS::UnderlinedGreen($sample) . ' ' );
$con->println( CS::BlinkingGreen($sample) );

$con->print( CS::DimYellow($sample) . ' ' );
$con->print( CS::Yellow($sample) . ' ' );
$con->print( CS::BoldYellow($sample) . ' ' );
$con->print( CS::UnderlinedYellow($sample) . ' ' );
$con->println( CS::BlinkingYellow($sample) );

$con->print( CS::DimBlue($sample) . ' ' );
$con->print( CS::Blue($sample) . ' ' );
$con->print( CS::BoldBlue($sample) . ' ' );
$con->print( CS::UnderlinedBlue($sample) . ' ' );
$con->println( CS::BlinkingBlue($sample) );

$con->print( CS::DimPurple($sample) . ' ' );
$con->print( CS::Purple($sample) . ' ' );
$con->print( CS::BoldPurple($sample) . ' ' );
$con->print( CS::UnderlinedPurple($sample) . ' ' );
$con->println( CS::BlinkingPurple($sample) );

$con->print( CS::DimCyan($sample) . ' ' );
$con->print( CS::Cyan($sample) . ' ' );
$con->print( CS::BoldCyan($sample) . ' ' );
$con->print( CS::UnderlinedCyan($sample) . ' ' );
$con->println( CS::BlinkingCyan($sample) );

$con->print( CS::DimWhite($sample) . ' ' );
$con->print( CS::White($sample) . ' ' );
$con->print( CS::BoldWhite($sample) . ' ' );
$con->print( CS::UnderlinedWhite($sample) . ' ' );
$con->println( CS::BlinkingWhite($sample) );

$con->println();
